In this project we are implementing a convolution neural network using optical components.

The architecture has two devices that matter: a digital micromirror device (DMD) and a spatial light modulator (SLM).
The DMD will display the weights of the neural network. Roughly speaking, each mirror of the DMD will transfer the digital information of the input. 
The SLM will transfer the information of the weights. The reason we are using this scheme is because there are very few weights (size of kernel is normally 3x3) so using the DMD for the input makes much more sense. Note this is different from the FC setup.

We can approximate number of timesteps needed by doing sum (size of kernel * num_filters). In general this will be small (hundreds) so it should only take a few seconds to run.

