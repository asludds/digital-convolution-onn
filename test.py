#This file allows for testing of functions that are written

import unittest
import numpy as np
from measurement_camera import measurement_camera

class TestMeasurementCamera(unittest.TestCase):
    def setUp(self):
        self.mc = measurement_camera()
        self.rows = 100
        self.cols = 100

    def test_measure_grid(self):
        # raise NotImplementedError("Measure Grid Test Not Implemented")
        pass

    def test_convert_binary_to_bytes_forward_zeros(self):
        binary_array_list = [np.zeros((self.rows,self.cols)) for i in range(8)]
        bytes_array = self.mc.convert_binary_to_bytes(binary_array_list,order="forward")
        perfect_zero_array = np.zeros((self.rows,self.cols))
        self.assertTrue(np.allclose(bytes_array,perfect_zero_array))

    def test_convert_binary_to_bytes_forward_ones(self):
        binary_array_list = [np.ones((self.rows,self.cols)) for i in range(8)]
        bytes_array = self.mc.convert_binary_to_bytes(binary_array_list,order="forward")
        perfect_255_array = 255*np.ones((self.rows,self.cols))
        self.assertTrue(np.allclose(bytes_array,perfect_255_array))

    def test_convert_binary_to_bytes_backward_zeros(self):
        binary_array_list = [np.zeros((self.rows,self.cols)) for i in range(8)]
        bytes_array = self.mc.convert_binary_to_bytes(binary_array_list,order="backward")
        perfect_zero_array = np.zeros((self.rows,self.cols))
        self.assertTrue(np.allclose(bytes_array,perfect_zero_array))

    def test_convert_binary_to_bytes_backward_ones(self):
        binary_array_list = [np.ones((self.rows,self.cols)) for i in range(8)]
        bytes_array = self.mc.convert_binary_to_bytes(binary_array_list,order="backward")
        perfect_255_array = 255*np.ones((self.rows,self.cols))
        self.assertTrue(np.allclose(bytes_array,perfect_255_array))

    def test_multiply_neighboring_values_zeros(self):
        #First we will create zero valued arrays
        array = np.zeros((self.rows,self.cols))
        #Then cast them to uint8
        array_8bit = np.array(array,dtype=np.uint8)
        #Apply function
        output = self.mc.multiply_neighboring_values(array_8bit)
        #Then we validate the 16bit returned value
        self.assertTrue(np.allclose(output,np.zeros((self.rows,int(self.cols/2)))))


    def test_multiply_neighboring_values_ones(self):
        #First we will create one valued arrays
        array = np.ones((self.rows,self.cols))
        #Then cast them to uint8
        array_8bit = np.array(array,dtype=np.uint8)
        #Apply function
        output = self.mc.multiply_neighboring_values(array_8bit)
        #Then we validate the 16bit returned value
        self.assertTrue(np.allclose(output,np.ones((self.rows,int(self.cols/2)))))

'''
In this test we are going to figure out the correct way to quantize a neural network.
'''
class TestNNQuantization(unittest.TestCase):
    #Train an MNIST NN and save the weights
    def setUp(self):
        self.mc = measurement_camera()

        #Check if there is already weights
        import os.path
        is_file = os.path.isfile("quantization_test/weights.h5")
        print(is_file)
        if is_file == True:
            from keras.models import load_model
            self.saved_model = load_model("quantization_test/weights.h5")
            return
        else:
            #Train MNIST NN
            from train_model import train_model_mnist
            self.saved_model = train_model_mnist(32,"Adam",save_model="quantization_test/weights.h5")

    def test_quantization_keras(self):
        #In this we are going to quantize the neural network, pass it through the quantization scheme, and then verify the output is reasonably close to when we just normally evalulate the neural network
        from train_model import evaluate_model_mnist
        original_performance = evaluate_model_mnist(self.saved_model)
        original_performance = original_performance[1]

        #Here we are going to make calls to the quantization class
        from quantization import quantization
        from train_model import get_data

        (x_train,y_train),(x_test,y_test) = get_data()

        weights = self.saved_model.get_weights()

        q = quantization(x_test,weights[0])

        output_convolve = q.convolve()

        #Create a quantization layer for fully connected layer

        qfc1 = quantization(output_convolve,weights[1])

        output_fc1 = qfc1.product(weights[2])

        qfc2 = quantization(output_fc1,weights[3])

        output_fc2 = qfc2.product(weights[4])

        ag = np.argmax(output_fc2)

        print(ag)

        self.assertTrue(True)


if __name__ == "__main__":
    unittest.main()
