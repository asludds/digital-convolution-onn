import ctypes
import time
import numpy as np
from PIL import Image

class dlp3000():
    def __init__(self):
        self.dll = ctypes.CDLL("./libneuralnet.so")

    #Wrapper for main function
    def dmd_main(self):
        self.dll.main()

    #Wrapper for Versions function
    def dmd_firmware_versions(self):
        self.dll.Versions()

    #Wrapper for Display_HDMI
    def dmd_display_hdmi(self):
        self.dll.Display_HDMI()

    #Wrapper for to open up dmd
    def dmd_open(self):
        self.dll.Open()

    #Wrapper for initialization
    def dmd_initialization(self,bitdepth,numpatterns):
        self.dll.Initialization(ctypes.c_int(bitdepth),ctypes.c_int(numpatterns))

    #Wrapper for DefineBMP
    def dmd_define_bmp(self,image_index,file_address):
        try:
            file_address.decode()
        except AttributeError:
            pass
        a = ctypes.c_char_p(file_address.encode())
        integer = ctypes.c_int(image_index)
        self.dll.DefineBMP(integer,a)

    def dmd_start_pattern(self):
        self.dll.StartPattern()

    def dmd_advance_pattern_sequence(self):
        self.dll.AdvancePatternSequence()

    def dmd_stop_pattern(self):
        self.dll.StopPattern()

    def dmd_close(self):
        self.dll.Close()

if __name__ == "__main__":
    x, y = 608, 684
    for i in range(8):
        if i % 2 == 0:
            array = np.zeros((y,x),"uint8")
        elif i % 2 == 1:
            array = 255*np.ones((y,x),"uint8")
        image = Image.fromarray(array)
        image = image.convert("1")
        image.save("./images/testing/"+str(i) + ".bmp")

    dmd = dlp3000()
    dmd.dmd_initialization(1,8)
    for i in range(8):
        string = "./images/testing/" + str(i) + ".bmp"
        dmd.dmd_define_bmp(i,string)
        
    dmd.dmd_start_pattern()

    for i in range(8):
        dmd.dmd_advance_pattern_sequence()
        time.sleep(2)
        print("ps")
    dmd.dmd_stop_pattern()
