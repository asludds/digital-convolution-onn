import numpy as np

'''
Copied from main.py:
Order of operations:
Initialization: Setup the DMD, SLM, laser diode. Run quick tests on each.
for each conv layer:
    for each kernel:
        for each value in the kernel:
            for i in range(8): #Here we are accounting for number of bits that we have quantized over
                Set the laser diode to send kernel_value bit i
                Set the DMD to send input bit i
                Measure the output and accumulate
            Take accumulated values, multiply element-wise and store for use in next layer

After this convolutional process we will run the FC NN offline on the received data.
Note that the file simulate.py simulates this encoding scheme to verify that it can work as we expect.
'''


'''
Our CNN weights are stored in 
'''