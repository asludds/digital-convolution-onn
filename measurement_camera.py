from pyueye_camera import ueye_camera
import numpy as np

class measurement_camera(ueye_camera):
    def __init__(self):
        super( measurement_camera,self).__init__(ID=0,subsample_x=8,subsample_y=8)

    '''
    This function takes a list of binary numpy arrays and uses this to create the corresponding floating point numpy array.
    The argument order denotes MSB vs LSB.
    If order is forward the first array has the MSB.
    If order backward the first array has the LSB.
    '''
    def convert_binary_to_bytes(self,binary_array_list,order="forward"):
        #We want to check that the input is 8 bits, otherwise error out
        assert len(binary_array_list) == 8
        if order == "forward":
            #The first array is worth 128 times the last
            temp = np.zeros(binary_array_list[0].shape)
            for i in range(8):
                temp += 2**i * binary_array_list[i]
            return temp

        elif order == "backward":
            #The first array is worth 1
            temp = np.zeros(binary_array_list[0].shape)
            for i in range(8):
                temp += 2**(7-i)*binary_array_list[i]
            return temp

        else:
            print("order is not backward or forward")
            assert False

    '''
    This function takes an array of 8 bit values and returns an array half of the size of each axis which representings neighboring values being multiplied together. The values in this array are 16 bit.
    '''
    def multiply_neighboring_values(self,floating_point_array):
        assert floating_point_array.shape[0]%2 == 0
        assert floating_point_array.shape[1]%2 == 0
        array_16bit = np.array(floating_point_array,dtype=np.uint16)
        output_16bit = np.multiply(array_16bit[:,::2],array_16bit[:,1::2])
        return output_16bit

if __name__ == "__main__":
    mc = measurement_camera()
    mc.measure_effective_framerate()
    mc.disconnect()