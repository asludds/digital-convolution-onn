import numpy as np

class quantization():
    def __init__(self,image,kernel):
        self.image = image
        self.kernel = kernel
        self.quantize()

    '''
    We are using the simple quantization scheme proposed by intel for their architectures:
    https://intel.github.io/mkl-dnn/ex_int8_simplenet.html
    '''
    def quantize(self):
        self.image_quantized = np.round(255/np.max(np.abs(self.image))*self.image)
        self.image_quantized = self.image_quantized.astype(np.uint8)
        self.kernel = np.round(255/np.max(np.abs(self.kernel))*self.kernel)
        self.kernel = self.kernel.astype(np.uint8)
        return

    def product(self, bias = None):
        #We want to assert that the product is indeed of the right type
        assert self.image.dtype == np.uint8
        assert self.kernel.dtype == np.uint8
        if bias == None:
            return np.dot(self.image,self.kernel)
        else:
            return np.dot(self.image,self.kernel) + bias

    def convolve(self):
        #Here we are going to convolve the image and the kernel. It also performs ReLU
        import scipy.signal as signal
        # signal.convolve(self.image,self.kernel)

        # return signal.convolve2d(self.image,kernel)
