import numpy as np
import keras
import keras
from keras.datasets import mnist
from keras.datasets import imdb
from keras.models import Sequential
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.preprocessing import sequence
from keras.layers.embeddings import Embedding
from scipy.misc import imread, imresize
from keras.models import model_from_json, load_model

import random
import os

def get_data():
    img_rows, img_cols = 28,28
    num_category = 10

    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    X_train = X_train.reshape(X_train.shape[0], 28, 28, 1)
    X_test = X_test.reshape(X_test.shape[0], 28, 28, 1)

    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')

    X_train/=255
    X_test/=255

    y_train = keras.utils.to_categorical(y_train, num_category)
    y_test = keras.utils.to_categorical(y_test, num_category)
    return (X_train,y_train),(X_test,y_test)

def train_model_mnist(batch_size,optimizer,save_model = None):
    batch_size = batch_size
    num_classes = 10
    epochs = 1000

    img_rows, img_cols = 28,28
    num_category = 10

    (X_train,y_train),(X_test,y_test)  = get_data()

    input_shape = (img_rows,img_cols,1)

    early_stopping = keras.callbacks.EarlyStopping(monitor='val_loss',
                              min_delta=0,
                              patience=2,
                              verbose=0, mode='auto')

    model = Sequential()

    #Note that we are going to do the convolutional operations online and the FC layers are going to be done offline.
    model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape,
                 use_bias = False))

    model.add(Flatten())

    model.add(Dense(128, activation='relu'))

    model.add(Dense(num_category, activation='softmax'))

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer= optimizer,
                metrics=['accuracy'])

    model.fit(X_train, y_train,
            batch_size=batch_size,
            epochs=epochs,
            verbose=1,
            validation_data=(X_test, y_test),
            callbacks = [early_stopping])

    score = model.evaluate(X_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    if save_model != None:
        model.save(save_model)

    return model

def evaluate_model_mnist(save_model):
    (x_train,y_train),(x_test,y_test)  = get_data()
    return save_model.evaluate(x_test,y_test)
