#This python project drives the digital convolution neural network project

#In this project we have several pieces of hardware that we will be using. 
# They have their own seperate python files because we have to debug and calibrate them. 
# These pieces of hardware are a DMD, SLM, and an LED driver, a measurement camera and a debugging camera.

from led_driver import led_driver
from debugging_camera import debugging_camera
from measurement_camera import measurement_camera
from laser_diode import laser_diode
from firmware import dlp3000
from nn_bitmap import *
from progress.bar import Bar

'''
Order of operations:
Initialization: Setup the DMD, SLM, laser diode. Run quick tests on each.
Quantization: Quantize the neural network. Allow for 
for each conv layer:
    for each kernel:
        for each value in the kernel:
            for i in range(8): #Here we are accounting for number of bits that we have quantized over
                Set the laser diode to send kernel_value bit i
                Set the DMD to send input bit i
                Measure the output and accumulate
            Take accumulated values, multiply element-wise and store for use in next layer

After this convolutional process we will run the FC NN offline on the received data.
Note that the file simulate.py simulates this encoding scheme to verify that it can work as we expect.
'''


'''
Initialization: Setting up the DMD and other hardware
'''

led = led_driver()
debug_camera = debugging_camera()
measure_camera = measurement_camera()
ld = laser_diode()

#Import a model that we will send
#TODO

#Images that DMD will store
number_of_bitmap_images, bitmap_locations = keras_model_to_bitmaps(keras_model)

dmd = dlp3000()
#Initialize the DMD and upload the images
dmd.dmd_initialization(1,number_of_bitmap_images)

bar = Bar("Uploading images to DMD memory",max=number_of_bitmap_images)
for i in range(number_of_bitmap_images):
    string = bitmap_locations + str(i) + ".bmp"
    dmd.dmd_define_bmp(i,string)
    bar.next()
bar.finish()

dmd.dmd_start_pattern()

'''
Transferring neural network weights
for each conv layer:
    for each kernel:
        for each value in the kernel:
            for i in range(8): #Here we are accounting for number of bits that we have quantized over
                Set the laser diode to send kernel_value bit i
                Set the DMD to send input bit i
                Measure the output and accumulate
            Take accumulated values, multiply element-wise and store for use in next layer
'''
#Layer 1
#Itterate through each kernel

#Layer 2

#Fully connected layers. Note that these are handled offline.









'''
Shutdown procedure
'''

dmd.dmd_stop_pattern()
