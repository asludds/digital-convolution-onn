# This document contains the LED driver

class led_driver():
    def __init__(self):
        self.state = "off"

    '''
    This function sets the state of the led. It takes as argument a string representing the new state of the led. The function calls update_led.
    '''
    def set_state(self,newstate):
        assert newstate=="on" or newstate=="off"
        self.state = newstate
        self.update_led()

    """
    This function takes the current state of the class and sets the led state to match that state
    """
    def update_led(self):
        pass #TODO
