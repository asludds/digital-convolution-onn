import serial
import time

class laser_diode():
    def __init__(self,COMPORT="COM3",baudrate=9600):
        self.serial = serial.Serial(COMPORT, baudrate, timeout=0)
    
    def turn_on(self):
        self.serial.write(str.encode("1"))

    def turn_off(self):
        self.serial.write(str.encode("0"))

    '''
    Make sure that when we finish executing the program that the laser diode is off
    '''
    def disconnect(self):
        self.serial.write(str.encode("0"))

if __name__ == "__main__":
    ld = laser_diode()
    ld.turn_off()
    time.sleep(5)
    ld.turn_on()
    time.sleep(5)
    ld.turn_off()